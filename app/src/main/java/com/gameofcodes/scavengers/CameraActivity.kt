package com.gameofcodes.scavengers

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.IOException
import java.time.LocalDateTime

class CameraActivity : AppCompatActivity(), View.OnClickListener{

    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()

    private val PICK_IMAGE_REQUEST = 1234
    private var filePath : Uri? = null
    internal var storage: FirebaseStorage?=null
    internal var storageReference:StorageReference?=null

    @SuppressLint("NewApi")
    val current = LocalDateTime.now()

    private val IMAGE_CAPTURE_CODE = 1001
    val PERMISSION_CODE = 1000

    var placeName : String ="Empty"
    var tagId : String = "Empty"

    override fun onClick(p0: View) {

        if(p0 === btn_choose)
            showFileChooser()
        else if (p0 ===btn_upload)
            uploadFile()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data!=null && data.data!=null)
        {
            filePath = data.data;
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                iv_photoUpload!!.setImageBitmap(bitmap)
            } catch (e:IOException){

                System.out.println("error is: ")
                e.printStackTrace()
            }
        }
        if(resultCode == Activity.RESULT_OK){
            //set Image in imageView
            iv_photoUpload.setImageURI(filePath)
        }
    }

    private fun uploadFile() {
        if(filePath !=null){
            val progressDialog = ProgressDialog(this)
            progressDialog.setTitle("Uploading....")
            progressDialog.show()

            //val imageRef = storageReference!!.child("images/"+ UUID.randomUUID().toString())
            val imageRef = storageReference!!.child("images/$current")
             imageRef.putFile(filePath!!)
                 .addOnSuccessListener {
                     progressDialog.dismiss()
                     Toast.makeText(applicationContext,"File Uploaded", Toast.LENGTH_SHORT).show()

                     val result = it.metadata!!.reference!!.downloadUrl!!
                     result.addOnSuccessListener {
                         val imageLink = it.toString()
                         Log.i("URL IS ->", imageLink)

                         //WRITE THE CODE TO UPLOAD THE LINK TO DATABASE
                         writeData("${auth.uid}","$placeName", "$tagId", imageLink)
                     }
                 }
                 .addOnFailureListener{
                     progressDialog.dismiss()
                     Toast.makeText(applicationContext,"Failed", Toast.LENGTH_SHORT).show()
                 }
                 .addOnProgressListener { taskSnapShot->
                     val progress = 100.0 * taskSnapShot.bytesTransferred/taskSnapShot.totalByteCount
                     progressDialog.setMessage("Uploaded " + progress.toInt() + "%...")
                 }
        }
    }

    private fun showFileChooser(){
        val intent = Intent()
        intent.type="image/*"
        intent.action= Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent,"Select Image"), PICK_IMAGE_REQUEST)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        //Init firebase
        storage = FirebaseStorage.getInstance()
        storageReference = storage!!.reference

        FirebaseApp.initializeApp(this)
        auth = FirebaseAuth.getInstance()

        //Following code getting Username value sent from previous page. Message has Username
        val bundle = intent.extras
        placeName = bundle!!.getString("message")
        Log.d("Getting uName here:" , "$placeName")

        val extras = intent.extras
        tagId = extras!!.getString("messageTag")

        //setup button
        btn_choose.setOnClickListener(this)
        btn_upload.setOnClickListener(this)
        btn_camera.setOnClickListener(){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(checkSelfPermission(android.Manifest.permission.CAMERA)== PackageManager.PERMISSION_DENIED
                    || checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED ){
                    // Permision not granted
                    val permissions = arrayOf(android.Manifest.permission.CAMERA,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE )
                    requestPermissions(permissions, PERMISSION_CODE)
                }
                else{
                    //Permission granted
                    openCamera()
                }
            }else{
                //system os < marshmallow
                openCamera()
            }
        }
    }

    //Sneh's code for camera
    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From Camera")
        filePath = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //Cameria Intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, filePath)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            PERMISSION_CODE -> {
                if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Permission from popup granted
                    openCamera()
                }
                else{
                    //Permission Denied
                    Toast.makeText(this,"Permission Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun writeData(uId: String, locationName: String, tagNo : String, imageUrl : String) {
        val tUserId = uId
        val historyTbl = db.collection("StoredImages")
        // Following are the fields in User's table in fireStore
        val docData = hashMapOf(
            "userId" to uId,
            "locationName" to locationName,
            "tagId" to tagNo,
            "imageUrl" to imageUrl
        )
        Log.d("Search history", "$uId, $locationName, $imageUrl")

        //Following code is putting it in database
        if(tUserId != null) {
            Log.d("Stored user status", "User credentials stored ${tUserId}")
            // storing user's credentials in fireStore in following middle collection type
            historyTbl.document("${auth.uid}_($tagNo)_$current").set(docData)
            //Toast.makeText(applicationContext, "Success", Toast.LENGTH_LONG).show()
        }
        else{
            Log.d("Stored user status", "User image info not stored successfully")
            Toast.makeText(applicationContext, "Failed, to store image in DB.", Toast.LENGTH_LONG).show()
        }
    }
}