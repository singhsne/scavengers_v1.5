package com.gameofcodes.scavengers

import android.os.Bundle
import android.provider.AlarmClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.all_scores.*
import kotlinx.android.synthetic.main.top_scores.*

class FragmentTopScores : Fragment() {
    companion object {
        private lateinit var recyclerView: RecyclerView
        private lateinit var auth: FirebaseAuth
        val db = FirebaseFirestore.getInstance()
        var currentUserName: String = ""
        fun newInstance(message: String): FragmentTopScores {
            val f = FragmentTopScores()
            val bdl = Bundle(1)
            bdl.putString(AlarmClock.EXTRA_MESSAGE, message)
            f.setArguments(bdl)
            return f

        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view: View? = inflater.inflate(R.layout.top_scores, container, false)
        recyclerView = view!!.findViewById(R.id.scorestopView)
        FirebaseApp.initializeApp(this!!.context!!)
        auth = FirebaseAuth.getInstance()
        readData() {
            currentUserName = "${it}"
            storeScoresHistory()
        }

        return view
    }
    fun storeScoresHistory(){
        val userAvatar: ArrayList<String> = ArrayList()
        val userUsername: ArrayList<String> = ArrayList()
        val userTotalPoints: ArrayList<Int> = ArrayList()

        FragmentAllScores.db.collection("Users")
            .orderBy("totalPoints", Query.Direction.DESCENDING)
            .limit(3)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    userAvatar.add(document.data["Avatar"].toString())
                    userUsername.add(document.data["UserName"].toString())
                    userTotalPoints.add(document.data["totalPoints"].toString().toInt())
                    println(userTotalPoints)
                }
                scorestopView.layoutManager = LinearLayoutManager(context)
                scorestopView.adapter = ScoresAdapter(userAvatar, userUsername, userTotalPoints, currentUserName )
            }
            .addOnFailureListener { exception ->
            }


    }
    fun readData(myCallback : (String) -> Unit) {
        FragmentAllScores.db.collection("Users").document("${auth.uid}")
            .get()
            .addOnSuccessListener { document ->
                val currentUser = (document.get("UserName")).toString()
                myCallback(currentUser)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents.", exception)
            }
    }
}