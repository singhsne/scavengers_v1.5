package com.gameofcodes.scavengers

import android.content.Intent
import android.os.Bundle
import android.util.Log

import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso

class ImageFullscreen : AppCompatActivity(){

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fullscreen)
        Log.e("IMAGEFULLSCREEN","ON CREATE METHOD")

      loadImage()

    }

   fun loadImage(){

       val imageLink = intent.getStringExtra("ImageLink")

       Log.e("FULLSCREEN",imageLink.toString())
       val postImage = findViewById<ImageView>(R.id.fullScreen)

       Picasso.get()
           .load(imageLink.toString())
           .placeholder((R.drawable.placeholder))
           .memoryPolicy(MemoryPolicy.NO_CACHE)
           .into(postImage)
       Log.e("FULLSCREEN","ON CREATE")

  }



    override fun onBackPressed() {
        val a = Intent(this, ImageActivity::class.java)
        Log.e("ONBACKPRESSED","clicked")

        startActivity(a)
        finish()
    }


}