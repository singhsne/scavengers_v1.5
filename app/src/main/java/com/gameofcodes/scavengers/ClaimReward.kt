package com.gameofcodes.scavengers
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.MediaScannerConnection
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_claim_reward.*
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import com.google.firebase.auth.FirebaseAuth

class ClaimReward : AppCompatActivity() {

    internal var bitmap: Bitmap? = null
    private var etqr: EditText? = null
    private var btn: Button? = null
    lateinit var ref: DatabaseReference
    private lateinit var authe: FirebaseAuth
    private val channelId = "com.gameofcodes.scavengers"
    private val description = "Notification"

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder

    var rcid = 0
    val TAG = "Message:"
    val db = FirebaseFirestore.getInstance()

    var rewardType : Int = 0
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_claim_reward)

        val iv = findViewById(R.id.iv) as ImageView
        btn = findViewById(R.id.btn) as Button
        val progressbar = findViewById(R.id.progressBar) as ProgressBar
        val rewardImage = findViewById(R.id.imageView) as ImageView
        val rewDesc = findViewById(R.id.txtDesc) as TextView
        val rewPoints = findViewById(R.id.txtPointsReq) as TextView

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        progressbar.visibility = View.GONE

        rewardType = intent.getIntExtra("rewardType", 0)

        if(rewardType.equals(1)){
            rewardImage.setImageResource(R.drawable.coffee2)
            rewDesc.text = "Coffee"
            rewPoints.text = "1000 Points"
        }else if(rewardType.equals(2)){
            rewardImage.setImageResource(R.drawable.bagel3)
            rewDesc.text = "Bagel"
            rewPoints.text = "1500 Points"
        }

        FirebaseApp.initializeApp(this)
        auth = FirebaseAuth.getInstance()


        btn!!.setOnClickListener {
            progressbar.visibility = View.VISIBLE
            iv.setImageResource(0)
            createNewClaimRewardRecord()
        }
    }

    fun checkScannedComplete(){
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                val post = dataSnapshot.getValue(ClaimReward::class.java)
                // ...
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        ref.addValueEventListener(postListener)
    }



    fun readData(myCallback: (List<Int>) -> Unit) {
        val rc = db.collection("RewardClaimed")
        rc.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val list = ArrayList<Int>()

                for (document in task.result!!) {
                    val rewardClaimID = Integer.parseInt(document.data["claimRewardID"].toString())
                    val isClaimedValue = document.data["claimRewardID"].toString()
                    list.add(rewardClaimID)

                }
                myCallback(list)
            }
        }
    }

    fun createNewClaimRewardRecord() {
        Log.d("Print UID", "${auth.uid}")
        readData {
            val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val currentDate = sdf.format(Date())
            var newRecordID: Int

            // val unusedBarcode = checkIfRewardClaimed()

            newRecordID = it.max()!!.plus(1)

            readUnusedBarcode{

                if(it.isEmpty()){
                    val record = hashMapOf(
                        "userID" to "${auth.uid}",
                        "claimRewardID" to newRecordID,
                        "isClaimed" to "false",
                        "date" to currentDate,
                        "rewardType" to rewardType
                    )

                    db.collection("RewardClaimed").document(newRecordID.toString()).set(record)
                }else{
                    newRecordID = it.max()!!.minus(1)
                }

                listenForChangesInRewardClaimed(newRecordID.toString(), "false")

                displayQRCode(newRecordID)
            }


        }
    }

    fun readUnusedBarcode(myCallback: (List<Int>) -> Unit) {
        val rc = db.collection("RewardClaimed")
        rc.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val list = ArrayList<Int>()

                for (document in task.result!!) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                    println("sdcds"+document.data.get("userID").toString())
//                    if(Integer.parseInt(document.data.get("userID").toString()) == 2){
//                        if(document.data.get("isClaimed").toString().equals("false")){
//                            println("safvsnfbsvfbvsjdfhsfdjfbsdfbksbdf")
//                            list.add(Integer.parseInt(document.data.get("claimRewardID").toString()))
//                        }
//                    }

                }
                myCallback(list)
            }
        }
    }

    fun checkIfRewardClaimed() : Int{
        var unusedBarcodeID = 0
        db.collection("RewardClaimed")
            .whereEqualTo("userID", 2)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                    if(document.data.get("isClaimed").toString().equals("false")){
                        unusedBarcodeID = Integer.parseInt(document.data.get("claimRewardID").toString())
                    }
                }

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
        return unusedBarcodeID

    }

    fun getPoints(){

        var points = 0
        val docRef = db.collection("Users").document("${auth.uid}")
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d(TAG, "DocumentSnapshot data: ${document.data?.get("currentpoints")}")
                    points = Integer.parseInt(document.data?.get("currentpoints").toString())
                } else {
                    Log.d(TAG, "No such document")
                }
                reducePoints(points)
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
            }

//        val docRef = db.collection("Users").document("FYu10Ll053UvrZ7POklKWZ4zldI3")
//        docRef.addSnapshotListener { snapshot, e ->
//            if (e != null) {
//                Log.w(TAG, "Listen failed.", e)
//                return@addSnapshotListener
//            }
//
//            val source = if (snapshot != null && snapshot.metadata.hasPendingWrites())
//                "Local"
//            else
//                "Server"
//
//            if (snapshot != null && snapshot.exists()) {
//                Log.d(TAG, "$source data: ${snapshot.data?.get("currentpoints")}")
//                var userCurrentScore = Integer.parseInt(snapshot.data?.get("currentpoints").toString())
////
//                reducePoints(userCurrentScore, rewardType)
//
//            } else {
//                Log.d(TAG, "$source data: null")
//            }
//        }

    }

    fun showNotification(){
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel =
                NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(this, channelId)
                .setContentTitle("Congratulations!")
                .setContentText("Enjoy! Keep tapping for more")
                .setSmallIcon(R.drawable.logo)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.reward11))
                .setContentIntent(pendingIntent)

        }else{
            builder = Notification.Builder(this)
                .setContentTitle("Congratulations!")
                .setContentText("Enjoy! Keep tapping for more")
                .setSmallIcon(R.drawable.logo)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.reward11))
                .setContentIntent(pendingIntent)
        }
        notificationManager.notify(1234, builder.build())
    }


    fun reducePoints(points: Int){

        var newpoints = 0

        if(rewardType.equals(1)){
            newpoints = points - 1000

            showNotification()

        }else if(rewardType.equals(2)){
            newpoints = points - 1500
            showNotification()
        }

        val ref = db.collection("Users").document("${auth.uid}")

        ref
            .update("currentpoints", newpoints)
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully updated!") }
            .addOnFailureListener { e -> println(Log.w(TAG, "Error updating document", e)) }

    }

    fun listenForChangesInRewardClaimed(newRecordID: String, columndefaultValue: String){

        val docRef = db.collection("RewardClaimed").document(newRecordID)
        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            val source = if (snapshot != null && snapshot.metadata.hasPendingWrites())
                "Local"
            else
                "Server"

            if (snapshot != null && snapshot.exists()) {

                if(!columndefaultValue.equals(snapshot.data?.get("isClaimed"))){
                    getPoints()
                    onBackPressed()
                }

                Log.d(TAG, "$source data: ${snapshot.data?.get("isClaimed")}")

            } else {
                Log.d(TAG, "$source data: null")
            }
        }

    }

    fun displayQRCode(newRecordID: Int){
        val progressbar = findViewById(R.id.progressBar) as ProgressBar
        val iv = findViewById(R.id.iv) as ImageView
        bitmap = TextToImageEncode(newRecordID.toString())
        iv.setImageBitmap(bitmap)

        progressbar.visibility = View.GONE
        Toast.makeText(this@ClaimReward, "QRCode created", Toast.LENGTH_SHORT).show()
    }


    @Throws(WriterException::class)
    private fun TextToImageEncode(Value: String): Bitmap? {
        val bitMatrix: BitMatrix
        try {
            bitMatrix = MultiFormatWriter().encode(
                Value,
                BarcodeFormat.QR_CODE,
                QRcodeWidth, QRcodeWidth, null
            )
        } catch (Illegalargumentexception: IllegalArgumentException) {
            return null
        }

        val bitMatrixWidth = bitMatrix.getWidth()
        val bitMatrixHeight = bitMatrix.getHeight()
        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

        for (y in 0 until bitMatrixHeight) {
            val offset = y * bitMatrixWidth

            for (x in 0 until bitMatrixWidth) {
                pixels[offset + x] = if (bitMatrix.get(x, y))
                    resources.getColor(R.color.black)
                else {
                    resources.getColor(R.color.white)
                }
            }
        }
        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)
        return bitmap
    }

    companion object {
        val QRcodeWidth = 500
        private val IMAGE_DIRECTORY = "/QRcodeDemonuts"
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.i(TAG, "onSaveInstanceState")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        Log.i(TAG, "onRestoreInstanceState")
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "onStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i(TAG, "onRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy")
    }
}