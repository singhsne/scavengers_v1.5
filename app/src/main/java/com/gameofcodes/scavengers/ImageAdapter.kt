package com.gameofcodes.scavengers

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.Gravity
import android.provider.AlarmClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import android.content.Intent.getIntent
import android.os.Bundle
import android.widget.PopupWindow
import androidx.core.content.ContextCompat.createDeviceProtectedStorageContext


class ImageAdapter(val userLocation: ArrayList<String>, val userImage: ArrayList<String>) : RecyclerView.Adapter<ImageAdapter.ViewHolder>()  {

    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()
    val imageRef = db.collection("StoredImages")
    var imageLink: ArrayList<String> = ArrayList()
    var imageLocation: ArrayList<String> = ArrayList()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_image, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userImage.size
    }

    override fun onBindViewHolder(holder: ImageAdapter.ViewHolder, position: Int) {
        //  holder.locations.text = userLocation[position]
        // holder.image.text = userImage[position]

        Picasso.get()
            .load(userImage[position])
            .placeholder((R.drawable.placeholder))
            .memoryPolicy(MemoryPolicy.NO_CACHE)
            .into(holder.image)

        //This on click function works for each image sending number. It pritns like 'Worked click? 3'
        holder.itemView.setOnClickListener{
            Log.d("something","Worked click? $position ")

            val intent = Intent(holder.itemView.context, ImageFullscreen::class.java)
            intent.putExtra("ImageLink",userImage[position] )
            holder.itemView.context.startActivity(intent)

            Log.d("UserImagePosition",userImage[position])

        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        // val locations: TextView = itemView.findViewById(R.id.txt_loc)

        val image: ImageView = itemView.findViewById(R.id.iv_photo)
    }
}