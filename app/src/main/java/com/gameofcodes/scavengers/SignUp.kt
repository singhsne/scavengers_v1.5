package com.gameofcodes.scavengers

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.transition.Slide
import android.transition.TransitionManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUp : AppCompatActivity() {
    private lateinit var authe: FirebaseAuth
    private val dbase = FirebaseFirestore.getInstance()

    private var uniqueId : String = ""
    private var tempFirstName : String = ""
    private var tempLastName : String = ""
    private var tempUserName : String = ""
    private var tempUserEmail : String = ""
    private var tempUserPhone : String = ""
    private var tempUserAvatar : String = "selectprof.png"  // Write user image(Avatar) code!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        // Firebase authorized user
        authe = FirebaseAuth.getInstance()

        // Register page's user's credentiatls
        val firstName = findViewById<EditText>(R.id.firstName)
        val lastName = findViewById<EditText>(R.id.lastName)
        val userName = findViewById<EditText>(R.id.uName)
        val userEmail = findViewById<EditText>(R.id.uEmail)
        val userPass = findViewById<EditText>(R.id.uPass)
        val userPhone = findViewById<EditText>(R.id.uPhone)
        val errorMessage = findViewById<TextView>(R.id.errorMessage)

        val btnRegister = findViewById<Button>(R.id.btnRegister)

        //Select Profile Picture
        imgBtn.setBackgroundResource(R.drawable.selectprof)

        //Clicking on image, display popup to select image
        imgBtn.setOnClickListener{
            // Initialize a new layout inflater instance
            val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            // Inflate a custom view using layout inflater
            val view = inflater.inflate(R.layout.choose_profilepic, null)

            // Initialize a new instance of popup window
            val popupWindow = PopupWindow(
                view, // Custom view to show in popup window
                LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                LinearLayout.LayoutParams.WRAP_CONTENT // Window height
            )

            // Set an elevation for the popup window
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                popupWindow.elevation = 10.0F
            }

            // If API level 23 or higher then execute the code
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                // Create a new slide animation for popup window enter transition
                val slideIn = Slide()
                slideIn.slideEdge = Gravity.TOP
                popupWindow.enterTransition = slideIn

                // Slide animation for popup window exit transition
                val slideOut = Slide()
                slideOut.slideEdge = Gravity.RIGHT
                popupWindow.exitTransition = slideOut

            }

            // Get the widgets reference from custom view
            val imgSelected1 = view.findViewById<ImageView>(R.id.img1)
            val imgSelected2 = view.findViewById<ImageView>(R.id.img2)
            val imgSelected3 = view.findViewById<ImageView>(R.id.img3)
            val imgSelected4 = view.findViewById<ImageView>(R.id.img4)
            val imgSelected5 = view.findViewById<ImageView>(R.id.img5)
            val imgSelected6 = view.findViewById<ImageView>(R.id.img6)
            val imgSelected7 = view.findViewById<ImageView>(R.id.img7)

            // Set click listener for popup window's text view
            imgSelected1.setOnClickListener{
                // Change the text color of popup window's text view
                tempUserAvatar = "img1"
                imgBtn.setBackgroundResource(R.drawable.img1)
                popupWindow.dismiss()
            }
            imgSelected2.setOnClickListener{
                // Change the text color of popup window's text view
                tempUserAvatar = "img2"
                imgBtn.setBackgroundResource(R.drawable.img2)
                popupWindow.dismiss()
            }
            imgSelected3.setOnClickListener{
                // Change the text color of popup window's text view
                tempUserAvatar = "img3"
                imgBtn.setBackgroundResource(R.drawable.img3)
                popupWindow.dismiss()
            }
            imgSelected4.setOnClickListener{
                // Change the text color of popup window's text view
                tempUserAvatar = "img4"
                imgBtn.setBackgroundResource(R.drawable.img4)
                popupWindow.dismiss()
            }
            imgSelected5.setOnClickListener{
                // Change the text color of popup window's text view
                tempUserAvatar = "img5"
                imgBtn.setBackgroundResource(R.drawable.img5)
                popupWindow.dismiss()
            }
            imgSelected6.setOnClickListener{
                // Change the text color of popup window's text view
                tempUserAvatar = "img6"
                imgBtn.setBackgroundResource(R.drawable.img6)
                popupWindow.dismiss()
            }
            imgSelected7.setOnClickListener{
                // Change the text color of popup window's text view
                tempUserAvatar = "img7"
                imgBtn.setBackgroundResource(R.drawable.img7)
                popupWindow.dismiss()
            }

            // Set a dismiss listener for popup window
            popupWindow.setOnDismissListener {
                Toast.makeText(applicationContext,"Profile Picture updated!",Toast.LENGTH_SHORT).show()
            }

            // Finally, show the popup window on app
            TransitionManager.beginDelayedTransition(root_layout)
            popupWindow.showAtLocation(
                root_layout, // Location to display popup window
                Gravity.LEFT, // Exact position of layout to display popup
                0, // X offset
                0 // Y offset
            )
        }

        //Creating new authentication for user and storing their credentials
        btnRegister.setOnClickListener {
            // Storing typed credentials in the strings
            tempFirstName = firstName.text.toString()
            tempLastName = lastName.text.toString()
            tempUserName = userName.text.toString()
            tempUserEmail = userEmail.text.toString()
            tempUserPhone = userPhone.text.toString()

            if(tempFirstName.isNullOrBlank() || tempLastName.isNullOrBlank()
                || tempUserName.isNullOrBlank()
                || tempUserEmail.isNullOrBlank() || userPass.text.toString().isNullOrBlank()
                || tempUserPhone.isNullOrBlank()){
                errorMessage.visibility = View.VISIBLE
            }
            else {
                // display progress bar
                val pd = ProgressDialog(this)
                pd.setMessage("Signing In")
                pd.show()
                //Creating account in Firebase
                createAccount(userEmail.text.toString(), userPass.text.toString())
                Log.d("StoredId second", "$uniqueId")

                // upon success, send user to mapsActivity page
                val myIntent = Intent(this, com.gameofcodes.scavengers.MapsActivity::class.java)
                myIntent.putExtra("message", "$tempUserName")
                pd.hide()
                this.startActivity(myIntent)
            }
        }
    }

    // Using firebase's creating user with email and password for auth and storing
    // their details in firestore
    private fun createAccount(email: String, password: String) {
        Log.d("Creating account", "createAccount:$email")
        // fun Firebase's createEmailAndPassword
        authe.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener() { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("SignUpStatus", "createUserWithEmail:success")
                    val user = authe.currentUser

                    uniqueId = (user!!.uid)
                    Log.d("StoredUid", "$uniqueId")

                    // Following writeData function stores user's info after
                    // creating user in Firebase auth
                    writeData(tempFirstName, tempLastName,tempUserName, tempUserEmail, tempUserPhone, tempUserAvatar, uniqueId) // Passing 1 as Avatar
                    println("Am i here?")
                    Log.d("Status", "Am i here?")
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d("SignUpStatus", "createUserWithEmail:failure", task.exception)
                    updateUI(null)
                }
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            Log.d("SignUpStatus", "Success for user with Uid:- ${user?.uid}")
        } else {
            //statusss?.setText("Error")
            Log.d("SignUpStatus", "Failed for user with Uid:- ${user?.uid}")
        }
    }

    fun writeData(firstName: String, lastName: String, userName: String, email: String, phone: String, avatar: String, uId: String) {
        val tUserId = uId
        val Users = dbase.collection("Users")
        // Following are the fields in User's table in fireStore
        val docData = hashMapOf(
            "FirstName" to firstName,
            "LastName" to lastName,
            "UserName" to userName,
            "Email" to email,
            "PhoneNum" to phone,
            "Avatar" to avatar,
            "currentpoints" to 10,
            "totalPoints" to 10
        )
        Log.d("Search", "$firstName, $lastName, $userName, $email, $phone, $avatar, $uId")

        if(tUserId != null) {
            Log.d("Stored user status", "User credentials stored ${tUserId}")
            // storing user's credentials in fireStore in document
            Users.document("$tUserId").set(docData)
            Toast.makeText(applicationContext, "Success", Toast.LENGTH_LONG).show()

        }
        else{
            Log.d("Stored user status", "User credentials not stored successfully")
            Toast.makeText(applicationContext, "Failed, try again later.", Toast.LENGTH_LONG).show()
        }
    }
}