package com.gameofcodes.scavengers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UsersAdapter(val userHistoryLocation: ArrayList<String>, val userHistoryPoints: ArrayList<String>) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.user_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
//        if (posts.size() < 1)
//            activity.recyclerView.setVisibility(View.GONE)
//        else {
//            activity.recyclerView.setVisibility(View.VISIBLE)
//            val params = activity.recyclerView.getLayoutParams()
//            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
//            activity.recyclerView.setLayoutParams(params)
//        }

        return userHistoryLocation.size
    }

    override fun onBindViewHolder(holder: UsersAdapter.ViewHolder, position: Int) {
        holder.locations.text = userHistoryLocation[position]
        holder.points.text = "+"+userHistoryPoints[position]
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val locations: TextView = itemView.findViewById(R.id.locations)
        val points: TextView = itemView.findViewById(R.id.points)

    }
}