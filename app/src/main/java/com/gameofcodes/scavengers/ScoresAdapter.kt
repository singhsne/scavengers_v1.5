package com.gameofcodes.scavengers

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.View.FOCUS_UP
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ScoresAdapter(val userAvatar: ArrayList<String>, val userUserName: ArrayList<String>, val userTotalPoints: ArrayList<Int>, val currentUserName: String) : RecyclerView.Adapter<ScoresAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.scores_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userTotalPoints.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val avatar: ImageView = itemView.findViewById(R.id.avatar)
        val username: TextView = itemView.findViewById(R.id.username)
        val totalpoints: TextView = itemView.findViewById(R.id.totalpoints)
        val rank : TextView = itemView.findViewById(R.id.rank)

    }
    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.rank.text = (position+1).toString()
        when(userAvatar[position]) {
            "img1" -> {
                holder.avatar.setImageResource(R.drawable.img1)
            }
            "img2" -> {
                holder.avatar.setImageResource(R.drawable.img2)
            }
            "img3" -> {
                holder.avatar.setImageResource(R.drawable.img3)
            }
            "img4" -> {
                holder.avatar.setImageResource(R.drawable.img4)
            }
            "img5" -> {
                holder.avatar.setImageResource(R.drawable.img5)
            }
            "img6" -> {
                holder.avatar.setImageResource(R.drawable.img6)
            }
            "img7" -> {
                holder.avatar.setImageResource(R.drawable.img7)
            }
        }
        if(currentUserName == userUserName[position]){
            holder.itemView.setBackgroundResource(R.drawable.border)
            //holder.itemView.scrollTo(position, position)
        }
        holder.username.text = userUserName[position]
        holder.totalpoints.text = userTotalPoints[position].toString()
    }
}

