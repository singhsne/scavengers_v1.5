package com.gameofcodes.scavengers

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_nfc_details.*
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*


class NfcDetails : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()

    //Finding Duplicates
    var duplicateUid = "Empty"
    var duplicateTagId = "Empty"
    var duplicateVisited = "" //Retrieving 'Yes' if already exists

    //Adding current and total points in Users
    var curPoints = 0
    var totPoints = 0

    //Retrieving data from Firestore
    var placeName = "Empty"
    var placeDescrition = "Empty"
    var placeAddress = "Empty"
    var placeGeo = "Empty"
    var placePoints = 0
    var placeImage = "Empty"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nfc_details)

        val extras = intent.extras
        val tagId = extras!!.getString("NfcTagId")
        val progressbar = findViewById(R.id.progBar) as ProgressBar
        progressbar.visibility = View.GONE

        Log.d("Intent", "$tagId is this")
        progressbar.visibility = View.VISIBLE

        FirebaseApp.initializeApp(this)
        auth = FirebaseAuth.getInstance()

        var tagHead = findViewById<TextView>(R.id.tagHeading)
        var tagDesc = findViewById<TextView>(R.id.tagDesc)
        var tagImg = findViewById<ImageView>(R.id.tagImg)
        var tagPoint = findViewById<TextView>(R.id.tagPoints)

        if(auth.uid == null) {
            Toast.makeText(applicationContext, "Please Log-in first", Toast.LENGTH_LONG).show()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        else {
            // Fiding duplicate value
            findDuplicateData() {
                Log.d("TAG", it.toString())
                tagHead.setText("$it")
                tagDesc.setText("")
                tagPoint.setText("!!!! 10 Points !!!!")

                if (duplicateVisited != "Yes") {
                    Log.d("Status", "Inside duplicateVisited")
                    readTagData() {
                        Log.d("TAG", it.toString())
                        Picasso.get().load("$placeImage").into(tagImg)
                        tagHead.setText("$it")
                        tagDesc.setText("$placeDescrition")
                        Toast.makeText(applicationContext, "New created!", Toast.LENGTH_LONG).show()
                        tagPoint.setText("!!!! $placePoints Points !!!!")
                        progressbar.visibility = View.GONE
                        //Excecuting writeData fun to write tag data in History for first time
                        writeData(
                            "${auth.uid}", "$placeName", "$placeGeo",
                            placePoints.toInt(), tagId
                        )

                        getOldPoints() {
                            Log.d("OldTotalPoints", it.toString())
                            Log.d("oldPoints", "$totPoints, $curPoints")
                            addPoints("${auth.uid}", totPoints, curPoints, placePoints)
                        }
                    }
                } else {
                    readTagData() {
                        //Just displaying information without writing
                        Log.d("TAG", it.toString())
                        Picasso.get().load("$placeImage").into(tagImg)
                        tagHead.setText("$it")
                        tagDesc.setText("$placeDescrition")
                        tagPoint.setText("Already tapped!")
                        progressbar.visibility = View.GONE
                        //Not writing in DB since History already updated. This is second time tapping
                    }
                    Toast.makeText(
                        applicationContext,
                        "You have already tapped!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
        takePicture.setOnClickListener(){
            val intent = Intent(this, CameraActivity::class.java)

            intent.putExtra("message", "$placeName")
            intent.putExtra("messageTag", "${tagId}")
            Log.d("PrintingToPass: ", "$placeName ${tagId}")
            startActivity(intent)
        }
        goHome.setOnClickListener(){
            val intent = Intent(this, MapsActivity::class.java)

            Log.d("PrintingToPass: ", "Lets go home!")
            startActivity(intent)
        }
    }

    fun readTagData(myCallback : (String) -> Unit) {
        Log.d("Print UID", "${auth.uid}")
        val extras = intent.extras
        val tagId = extras!!.getString("NfcTagId")

        db.collection("Tags").document("$tagId")
            .get()
            .addOnSuccessListener { document ->
                println(document.get("Place"))
                placeName = (document.get("Place")).toString()
                placeDescrition = (document.get("Description")).toString()
                placeAddress = (document.get("Address")).toString()
                placeGeo = (document.get("Location")).toString()
                placePoints = Integer.parseInt((document.get("Points")).toString())
                placeImage = (document.get("Image")).toString()

                placeDescrition = placeDescrition.replace("\\n", "\n")

                Log.d("This is Place: " , "$placeName")
                Log.d("All tag info", "$placeName, $placeAddress, $placePoints, $placeDescrition")
                myCallback(placeName)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents.", exception)
            }
    }
    fun writeData(uId: String, locationName: String, GeoLocation: String, points: Int, tagNo : String) {
        val tUserId = uId
        val historyTbl = db.collection("History")
        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        val currentDate = sdf.format(Date())
        // Following are the fields in User's table in fireStore
        val docData = hashMapOf(
            "userId" to uId,
            "locationName" to locationName,
            "pointsEarned" to points,
            "locationCoordinates" to GeoLocation,
            "Visited" to "Yes",
            "date" to currentDate,
            "tagId" to tagNo
        )
        Log.d("Search history", "$uId, $locationName, $points, $GeoLocation")
        Log.d("Stored user status", "User credentials stored ${tUserId}")
        // storing user's credentials in fireStore in document
        historyTbl.document("${auth.uid}_$tagNo").set(docData)
        Toast.makeText(applicationContext, "Success", Toast.LENGTH_LONG).show()

    }

    fun findDuplicateData(myCallback : (String) -> Unit) {
        Log.d("Print UID", "${auth.uid}")
        val extras = intent.extras
        val tagId = extras!!.getString("NfcTagId")

        db.collection("History").document("${auth.uid}_$tagId")
            .get()
            .addOnSuccessListener { document ->
                println(document.get("userId"))
                duplicateUid = (document.get("locationName")).toString()
                duplicateTagId = (document.get("tagId")).toString()
                duplicateVisited = (document.get("Visited")).toString()

                Log.d("FoundDuplicates: " , "$duplicateUid , $duplicateTagId")
                myCallback(duplicateUid)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error getting documents.", exception)
            }
    }

    //Get old points from Users table
    fun getOldPoints(myCallback : (Int) -> Unit) {
        Log.d("Print UID", "${auth.uid}")
        val extras = intent.extras
        val tagId = extras!!.getString("NfcTagId")

        db.collection("Users").document("${auth.uid}")
            .get()
            .addOnSuccessListener { document ->
                curPoints = Integer.parseInt((document.get("currentpoints")).toString())
                totPoints = Integer.parseInt((document.get("totalPoints")).toString())

                Log.d("Points: " , "$curPoints , $totPoints")
                myCallback(totPoints)
            }
            .addOnFailureListener { exception ->
                Log.w("Error", "Error Adding points!", exception)
            }
    }

    //Add points in Users table
    fun addPoints(uId: String,totalPoints: Int, currentPoints: Int, tagPoints: Int){
        val tUserId = uId
        val updateUserPoint = db.collection("Users")

        //Calculation of adding points
        curPoints = currentPoints + tagPoints
        totPoints = totalPoints + tagPoints

        Log.d("CheckPoints", "$uId, Current:- $curPoints, Total :-$totPoints")

        if(tUserId != null) {
            Log.d("Stored user points", "User points stored ${tUserId}")
            // storing user's new points in fireStore in document
            updateUserPoint.document("${auth.uid}")
                .update("currentpoints", curPoints, "totalPoints",totPoints)
            Toast.makeText(applicationContext, "Added points", Toast.LENGTH_LONG).show()
        }
        else{
            Log.d("NotAddedPoints", "User Points not stored")
            Toast.makeText(applicationContext, "Failed, to add points!", Toast.LENGTH_LONG).show()
        }
    }
}